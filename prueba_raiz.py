#importo la funcion para calcular la raiz cuadrada con un cierto error almacenada en el modulo calculos y math.py para el valor de python de la raiz cuadrada 

import math
from calculos import raiz

#defino un par de funciones para repetir codigo util
def raiz_compleja(z):
	if z < 0.0:
		print "El metodo no aproxima valores complejos"
		print "Desea calcular la raiz de {numero} si o no?".format(numero=abs(z))
		pregunta = raw_input()
		if pregunta == "si" or pregunta == "yes":
			z = abs(z)
		else:
			exit()
	return z

def validacion_err(x,y):
	if y < 0.0:
		print "El algoritmo entra en ciclo infinito; se tomo el valor absoluto!"
		y = abs(y)
	if y >= x/2.0:
		print "El metodo de aproximacion no es valido"
		exit()
	if y >= 1.0:
		print "La aproximacion puede ser muy erronea, pruebe valores entre 0 y 1!"
	return y

print "Ingrese el numero real al que le desea calcular la raiz cuadrada:"
#Manejo el caso que indique un parametro no valido
try:
	x = float(raw_input())
#Caso en que quiera aproximar complejos, la funcion no puede hacerlo
	x = raiz_compleja(x)
#Se da la opcion de volver a meter algun parametro valido
except ValueError or TypeError:
	print "Debe de especificar un numero"
	try: 
		x = float(raw_input("Digite el numero: "))
		x = raiz_compleja(x)
	except ValueError or TypeError:
		print "No especifico un numero!"
		exit()

print "Cual es su valor de convergencia deseado?"
#Manejo el caso que indique un parametro no valido
try:
	err = float(raw_input())
	err = validacion_err(x,err)
#Se da la opcion de volver a meter algun parametro valido
except ValueError or TypeError:
	print "Error debe de especificar un numero"
	try: 
		err = float(raw_input())
		err = validacion_err(x,err)
	except ValueError or TypeError:
		print "No especifico un numero"
		exit()
#Se despliega el valor aproximado y este se compara con el valor de python
print "La raiz de {x} es aproximadamente {raiz}".format(x=x, raiz=raiz(x, err))
print "El valor que da python es {raiz_python}".format(raiz_python=math.sqrt(x))
