
def calcular_pi(nmax):
     i=1.0
     p=1.0
     while i<=nmax:
          p *= (2.0*i/(2.0*i-1.0))*(2.0*i/(2.0*i+1.0))
          i += 1.0
     return 2.0*p

def raiz(x, err):
     est1 = x
     est2 = x/2.0
     while abs(est1-est2)>err:
         est1 = est2
         coc = x/est1
         est2 = (coc+est1)/2.0
     return est2

