import sys

#importo la funcion para calcular pi almacenada en el modulo calculos y math para el valor de python de pi

import math
from calculos import calcular_pi

#verifico que utilicen un parametro al llamar al programa y que este no sea negativo ni cero ni float, manejo la excepcion si su parametro de entrada no es un integer
try:
	if int(sys.argv[1])<=0 or not len(sys.argv) == 2:
		print "Error: debe especificar un numero entero positivo diferente de cero como parametro!"
	else:
		aproximacion = calcular_pi(int(sys.argv[1])) 
		print "Valor calculado por producto de Wallis hasta n={n}: {pi}".format(pi=aproximacion, n=sys.argv[1]) 
#Comparo con el valor de python para pi
		print "El valor dado por python es {pi_python}".format(pi_python=math.pi)
except ValueError:
	print "Debe de dar un numero entero como parametro!"
except IndexError:
	print "No especifico un parametro!"
	

